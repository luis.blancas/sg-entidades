package com.mx.service.segurenta;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class  ExceptionSegurenta   
{
	private int codigo;
	private String mensaje;
}
