package com.mx.service.segurenta.controller.catalogos;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import com.mx.service.segurenta.Bean;
import com.mx.service.segurenta.ExceptionSegurenta;
import com.mx.service.segurenta.Tools;
import com.mx.service.segurenta.entity.Users;
import com.mx.service.segurenta.entity.catalogos.Banco;
import com.mx.service.segurenta.entity.catalogos.Estatus;
import com.mx.service.segurenta.entity.catalogos.TipoPersona;
import com.mx.service.segurenta.entity.catalogos.TipoUsuario;
import com.mx.service.segurenta.entity.reglas.Cuenta;
import com.mx.service.segurenta.entity.reglas.Persona;
import com.mx.service.segurenta.repository.catalogos.BancoRepository;
import com.mx.service.segurenta.repository.catalogos.EstatusRepository;
import com.mx.service.segurenta.repository.catalogos.TipoPersonaRepository;
import com.mx.service.segurenta.repository.catalogos.TipoUsuarioRepository;
import com.mx.service.segurenta.repository.catalogos.UserRepository;
import com.mx.service.segurenta.repository.reglas.CuentaRepository;
import com.mx.service.segurenta.repository.reglas.PersonaRepository;

@RestController
@RequestMapping("/catalogos/banco")
@CrossOrigin
public class BancoController 
{

	@Autowired
	BancoRepository repo;
	@Autowired
	UserRepository usersRepo;
	
	@Autowired
	TipoPersonaRepository tipoPersonaRepo;
	
	@Autowired
	EstatusRepository estatusRepo;
	
	@Autowired
	TipoUsuarioRepository tipousuarioRepo;
	
	@Autowired
	PersonaRepository  personaRepo;
	
	@Autowired
	CuentaRepository  cunetaRepo;
	
	@GetMapping("/{id}")
	public ResponseEntity<?> obtieneCatalogo(@PathVariable long id) 
	{
		String mensaje="";
		try
		{
			Optional<Banco> optional= repo.findById(id);
			if(optional!=null)
			{
				try
				{
					Banco bean = optional.get();
					return new ResponseEntity<>(bean, HttpStatus.OK);
				}
				catch(Exception eV)
				{
					return new ResponseEntity<>((new ExceptionSegurenta(200,"Sin datos para id["+id+"]")),HttpStatus.OK);
				}
			}
			else
				return new ResponseEntity<>((new ExceptionSegurenta(200,"Sin datos para id["+id+"]")),HttpStatus.OK);
		}
		catch(Exception ex)
		{
			mensaje =  ex.getMessage();
		}
		return new ResponseEntity<>((new ExceptionSegurenta(400,mensaje)),HttpStatus.BAD_REQUEST);
	}
	@GetMapping("/")
	public ResponseEntity<?> obtieneCatalogos() 
	{
		String mensaje="";
		try
		{
			List<Banco> lista= repo.findAll();
			if(lista.size()==0)
				return new ResponseEntity<>((new ExceptionSegurenta(200,"Sin datos en tabla")),HttpStatus.OK);
			return new ResponseEntity<>(lista, HttpStatus.OK);
		}catch(Exception ex)
		{
			mensaje=ex.getMessage();
		}
		return new ResponseEntity<>((new ExceptionSegurenta(400,mensaje)),HttpStatus.BAD_REQUEST);
	}
	@Transactional
	@PostMapping("/")
	public ResponseEntity<?> salvarCatalogo(@RequestBody Bean bean) 
	{
		String mensaje="";
		try
		{
			if(bean.getDescripcion()==null)
			{
				return new ResponseEntity<>((new ExceptionSegurenta(400,"El campo descripcion es requerido")),HttpStatus.BAD_REQUEST);
			}
			else
			{
				if(bean.getDescripcion().length()>250)
				{
					return new ResponseEntity<>((new ExceptionSegurenta(400,"La longitud del campo descripcion debe ser menor a 250 ")),HttpStatus.BAD_REQUEST);
				}	
				if(bean.getDescripcion().length()==0)
				{
					return new ResponseEntity<>((new ExceptionSegurenta(400,"El campo descripcion debe contener algun valor ")),HttpStatus.BAD_REQUEST);
				}
			}
			mensaje =  "Error. Usuario "+bean.getLogin()+" No existe para relacionar nuevo catalogo";
			Optional<Users> optionalUser= usersRepo.findByUsername(bean.getLogin());
			Users usuario = optionalUser.get();
			String loginFinal= usuario.getUsername();
			if(loginFinal.equals(bean.getLogin())) 
				mensaje ="";
			else
				return new ResponseEntity<>((new ExceptionSegurenta(400,mensaje)),HttpStatus.BAD_REQUEST);
			Banco beanFinnal = repo.save(new Banco(bean.getDescripcion(),bean.getCorta() ,bean.getClave(),usuario));
			return new ResponseEntity<>(beanFinnal, HttpStatus.OK);
		}
		catch(Exception ex)
		{
			mensaje=ex.getMessage();
		}
		return new ResponseEntity<>((new ExceptionSegurenta(400,mensaje)),HttpStatus.BAD_REQUEST);
	}
	
	@Transactional
	@PostMapping(
			path="/banco/")
	public ResponseEntity<?> llenarCatalogoBanco()
	{
		try
		{
			Optional<Users> optionalUser= usersRepo.findByUsername("admin");
			Users usuario = optionalUser.get();
			String loginFinal= usuario.getUsername();
			
			Banco b = null;
			b=repo.save(new Banco("Banco Nacional de Mexico, S.A., Institucion de Banca Multiple, Grupo Financiero Banamex","BANAMEX ","002",usuario));
			b=repo.save(new Banco("BBVA Bancomer, S.A., Institucion de Banca Multiple, Grupo Financiero BBVA Bancomer","BBVA BANCOMER","012",usuario));
			b=repo.save(new Banco("Banco Santander (Mexico), S.A., Institucion de Banca Multiple, Grupo Financiero Santander","SANTANDER","014",usuario));
			b=repo.save(new Banco("HSBC Mexico, S.A., institucion De Banca Multiple, Grupo Financiero HSBC","HSBC","021 ",usuario));;
			
			return obtieneCatalogos();
		}
		catch(Exception e)
		{
			return new ResponseEntity<>((String)e.getMessage() ,HttpStatus.BAD_REQUEST);
		}
	}
	@Transactional
	@PostMapping(
			path="/persona/")
	public ResponseEntity<?> creaPersona()
	{
		try
		{
			Users usuario = (Users)Tools.getBean(usersRepo.findByUsername("admin"));
			Banco banco= (Banco)Tools.getBean(repo.findByClave("002"));
			
			Estatus activo = estatusRepo.save(new Estatus("Activo",usuario));
			Estatus inactivo = estatusRepo.save(new Estatus("Inactivo",usuario));
			
			TipoUsuario broker = tipousuarioRepo.save(new TipoUsuario("Broker",usuario));
			TipoUsuario propietario = tipousuarioRepo.save(new TipoUsuario("Propietario",usuario));
			TipoUsuario inquilino = tipousuarioRepo.save(new TipoUsuario("Inquilino",usuario));
			
			TipoPersona individual = tipoPersonaRepo.save(new TipoPersona("Individual",usuario));
			TipoPersona juridica = tipoPersonaRepo.save(new TipoPersona("Juridica",usuario));
			
			Persona person=new Persona(usuario, "02394802948230948203498", "Mexicana", "BABL791225", banco, individual);
			Persona nuevaPersona =  personaRepo.save(person); 
			
			Cuenta cuenta=new Cuenta(usuario,propietario,nuevaPersona);
			Cuenta cuentaNueva =  cunetaRepo.save(cuenta);
			
			
			return obtieneCatalogos();
		}
		catch(Exception e)
		{
			return new ResponseEntity<>((String)e.getMessage() ,HttpStatus.BAD_REQUEST);
		}
	}
	
	
}
