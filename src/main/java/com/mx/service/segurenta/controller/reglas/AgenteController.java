package com.mx.service.segurenta.controller.reglas;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import com.mx.service.segurenta.ExceptionSegurenta;
import com.mx.service.segurenta.bo.reglas.AgenteBO;
import com.mx.service.segurenta.Tools;
import com.mx.service.segurenta.entity.Users;
import com.mx.service.segurenta.entity.catalogos.*;
import com.mx.service.segurenta.entity.reglas.*;
import com.mx.service.segurenta.repository.catalogos.EstatusRepository;
import com.mx.service.segurenta.repository.catalogos.SucursalRepository;
import com.mx.service.segurenta.repository.catalogos.UserRepository;
import com.mx.service.segurenta.repository.reglas.*; 
@RestController
@RequestMapping("/services/agente")
@CrossOrigin
public class AgenteController 
{

	@Autowired
	AgenteRepository   agenteRepo;
	@Autowired
	EstatusRepository  estatusRepo;
	@Autowired
	PersonaRepository  personaRepo;
	@Autowired
	SucursalRepository sucursalRepo;
	@Autowired
	UserRepository     usersRepo;
	@GetMapping("/{id}")
	public ResponseEntity<?> obtieneServicio(@PathVariable long id) 
	{
		String mensaje="";
		try
		{
			Optional<Agente> optional= agenteRepo.findById(id);
			if(optional!=null)
			{
				try
				{
					Agente bean = optional.get();
					return new ResponseEntity<>(bean, HttpStatus.OK);
				}
				catch(Exception eV)
				{
					return new ResponseEntity<>((new ExceptionSegurenta(200,"AgenteController.Sin datos para id["+id+"]")),HttpStatus.OK);
				}
			}
			else
				return new ResponseEntity<>((new ExceptionSegurenta(200,"AgenteController.Sin datos para id["+id+"]")),HttpStatus.OK);
		}
		catch(Exception ex)
		{
			mensaje = "AgenteController."+ ex.getMessage();
		}
		return new ResponseEntity<>((new ExceptionSegurenta(400,mensaje)),HttpStatus.BAD_REQUEST);
	}
	@GetMapping("/")
	public ResponseEntity<?> obtieneServicios() 
	{
		String mensaje="";
		try
		{
			List<Agente> lista= agenteRepo.findAll();
			if(lista.size()==0)
				return new ResponseEntity<>((new ExceptionSegurenta(200,"AgenteController.Sin datos en tabla")),HttpStatus.OK);
			return new ResponseEntity<>(lista, HttpStatus.OK);
		}catch(Exception ex)
		{
			mensaje="AgenteController."+ex.getMessage();
		}
		return new ResponseEntity<>((new ExceptionSegurenta(400,mensaje)),HttpStatus.BAD_REQUEST);
	}
	@Transactional
	@PutMapping("/")
	public ResponseEntity<?> salvarServicio(@RequestBody AgenteBO bean) 
	{
		Users usuario=null;
		try
		{
			usuario = (Users) Tools.getBean(usersRepo.findByUsername(bean.getLogin()));
		}
		catch(Exception ex)
		{
			try
			{
				bean.setLogin("admin");
				usuario = (Users) Tools.getBean(usersRepo.findByUsername(bean.getLogin()));
			}
			catch(Exception ex1)
			{
				return new ResponseEntity<>((new ExceptionSegurenta(400,"AgenteController.Problemas al guardar con el usuario :"+bean.getLogin())),HttpStatus.BAD_REQUEST);
			}
		}
		Estatus  estatus=null;
		Persona  persona=null;
		Sucursal sucursal=null;
		try
		{
			estatus= (Estatus)  Tools.getBean(estatusRepo.findById(bean.getEstatus()));
		}
		catch(Exception ex)
		{
			return new ResponseEntity<>((new ExceptionSegurenta(400,"AgenteController.Problemas con Estatus ")),HttpStatus.BAD_REQUEST);
		}
		try
		{
			persona=(Persona)  Tools.getBean(personaRepo.findById(bean.getEstatus()));
		}
		catch(Exception ex)
		{
			return new ResponseEntity<>((new ExceptionSegurenta(400,"AgenteController.Problemas con Persona")),HttpStatus.BAD_REQUEST);
		}
		try
		{
			sucursal=(Sucursal)  Tools.getBean(sucursalRepo.findById(bean.getSucursal()));
		}
		catch(Exception ex)
		{
			return new ResponseEntity<>((new ExceptionSegurenta(400,"")),HttpStatus.BAD_REQUEST);
		}
		try
		{
			Agente entity =new Agente();
			entity.setCreado(Tools.getTimeStamp());
			entity.setModificado(Tools.getTimeStamp());
			entity.setCreadopor(usuario);
			entity.setModificadopor(usuario);
			entity.setIdestatus(estatus);
			entity.setIdpersona(persona);
			entity.setIdsucursal(sucursal);
			Agente entityNuevo  = agenteRepo.save(entity);
			return new ResponseEntity<>(entityNuevo, HttpStatus.OK);
		}
		catch(Exception e)
		{
			return new ResponseEntity<>((new ExceptionSegurenta(400,"AgenteController."+e.getMessage())),HttpStatus.BAD_REQUEST);	
		}
	}
	@Transactional
	@PostMapping("/")
	public ResponseEntity<?> modificarServicio(@RequestBody AgenteBO bean) 
	{
		Users usuario=null;
		Agente entity =null;
		try
		{
			entity = (Agente) Tools.getBean(agenteRepo.findById(bean.getId()));
		}
		catch(Exception ex)
		{
			return new ResponseEntity<>((new ExceptionSegurenta(400,"AgenteController.No existe Agente ["+bean.getId()+"")),HttpStatus.BAD_REQUEST);
		}
		try
		{
			usuario = (Users) Tools.getBean(usersRepo.findByUsername(bean.getLogin()));
		}
		catch(Exception ex)
		{
			try
			{
				bean.setLogin("admin");
				usuario = (Users) Tools.getBean(usersRepo.findByUsername(bean.getLogin()));
			}
			catch(Exception ex1)
			{
				return new ResponseEntity<>((new ExceptionSegurenta(400,"AgenteController.Problemas al guardar con el usuario :"+bean.getLogin())),HttpStatus.BAD_REQUEST);
			}
		}
		Estatus  estatus=null;
		Persona  persona=null;
		Sucursal sucursal=null;
		try
		{
			estatus= (Estatus)  Tools.getBean(estatusRepo.findById(bean.getEstatus()));
			entity.setIdestatus(estatus);
			entity=agenteRepo.save(entity);
		}
		catch(Exception ex)
		{
			return new ResponseEntity<>((new ExceptionSegurenta(400,"AgenteController.Problemas con Estatus ")),HttpStatus.BAD_REQUEST);
		}
		try
		{
			persona=(Persona)  Tools.getBean(personaRepo.findById(bean.getEstatus()));
			entity.setIdpersona(persona);
			entity=agenteRepo.save(entity);
		}
		catch(Exception ex)
		{
			return new ResponseEntity<>((new ExceptionSegurenta(400,"AgenteController.Problemas con Persona")),HttpStatus.BAD_REQUEST);
		}
		try
		{
			sucursal=(Sucursal)  Tools.getBean(sucursalRepo.findById(bean.getSucursal()));
			entity.setIdsucursal(sucursal);
			entity=agenteRepo.save(entity);
		}
		catch(Exception ex)
		{
			return new ResponseEntity<>((new ExceptionSegurenta(400,"")),HttpStatus.BAD_REQUEST);
		}
		try
		{
			
			entity.setModificado(Tools.getTimeStamp());
			entity.setModificadopor(usuario);
			entity  = agenteRepo.save(entity);
			return new ResponseEntity<>(entity, HttpStatus.OK);
		}
		catch(Exception e)
		{
			return new ResponseEntity<>((new ExceptionSegurenta(400,"AgenteController."+e.getMessage())),HttpStatus.BAD_REQUEST);	
		}
	}
}