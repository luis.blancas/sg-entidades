package com.mx.service.segurenta.controller.reglas;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import com.mx.service.segurenta.ExceptionSegurenta;
import com.mx.service.segurenta.bo.reglas.CuentaBO;
import com.mx.service.segurenta.entity.Users;
import com.mx.service.segurenta.entity.reglas.*;
import com.mx.service.segurenta.repository.catalogos.UserRepository;
import com.mx.service.segurenta.repository.reglas.CuentaRepository; 
@RestController
@RequestMapping("/services/cuenta")
@CrossOrigin
public class CuentaController {
	@Autowired
	CuentaRepository repo;
	@Autowired
	UserRepository usersRepo;
	@GetMapping("/{id}")
	public ResponseEntity<?> obtieneServicio(@PathVariable long id) 
	{
		String mensaje="";
		try
		{
			Optional<Cuenta> optional= repo.findById(id);
			if(optional!=null)
			{
				try
				{
					Cuenta bean = optional.get();
					return new ResponseEntity<>(bean, HttpStatus.OK);
				}
				catch(Exception eV)
				{
					return new ResponseEntity<>((new ExceptionSegurenta(200,"Sin datos para id["+id+"]")),HttpStatus.OK);
				}
			}
			else
				return new ResponseEntity<>((new ExceptionSegurenta(200,"Sin datos para id["+id+"]")),HttpStatus.OK);
		}
		catch(Exception ex)
		{
			mensaje =  ex.getMessage();
		}
		return new ResponseEntity<>((new ExceptionSegurenta(400,mensaje)),HttpStatus.BAD_REQUEST);
	}
	@GetMapping("/")
	public ResponseEntity<?> obtieneServicios() 
	{
		String mensaje="";
		try
		{
			List<Cuenta> lista= repo.findAll();
			if(lista.size()==0)
				return new ResponseEntity<>((new ExceptionSegurenta(200,"Sin datos en tabla")),HttpStatus.OK);
			return new ResponseEntity<>(lista, HttpStatus.OK);
		}catch(Exception ex)
		{
			mensaje=ex.getMessage();
		}
		return new ResponseEntity<>((new ExceptionSegurenta(400,mensaje)),HttpStatus.BAD_REQUEST);
	}
	@Transactional
	@PostMapping("/")
	public ResponseEntity<?> salvarServicio(@RequestBody CuentaBO bean) 
	{
		String mensaje="";
		try
		{
			//
			Optional<Users> optionalUser= usersRepo.findByUsername(bean.getLogin());
			Users usuario = optionalUser.get();
			String loginFinal= usuario.getUsername();
			if(loginFinal.equals(bean.getLogin())) 
				mensaje ="";
			else
				return new ResponseEntity<>((new ExceptionSegurenta(400,mensaje)),HttpStatus.BAD_REQUEST);
			Cuenta beanFinnal =null;// = repo.save(new Banco(bean.getDescripcion(),usuario));
			return new ResponseEntity<>(beanFinnal, HttpStatus.OK);
		}
		catch(Exception ex)
		{
			mensaje=ex.getMessage();
		}
		return new ResponseEntity<>((new ExceptionSegurenta(400,mensaje)),HttpStatus.BAD_REQUEST);
	}
}
