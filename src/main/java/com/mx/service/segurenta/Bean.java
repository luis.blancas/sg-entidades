package com.mx.service.segurenta;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Bean 
{

	String descripcion ;
	String clave;
	String corta;
	String login;
}
