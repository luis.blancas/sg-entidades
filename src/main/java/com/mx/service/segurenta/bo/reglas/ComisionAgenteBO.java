package com.mx.service.segurenta.bo.reglas;
import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ComisionAgenteBO  implements Serializable
{
	private static final long serialVersionUID = 8334290105021947380L;
	private Long      id;
    private Long      agente;
	private double    porcentajecomision;
	private double    montocomision;
	private Integer   pagado;
	private String    login;
}
