package com.mx.service.segurenta.bo.reglas;
import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class PropiedadBO   implements Serializable
{
	private static final long serialVersionUID = 8334290105021947380L;
	private Long    id;
    private Long    tipopropiedad;
    private String  nombre;
    private Double  precio;
    private Double  preciorenta;
    private Double  mantenimiento;
    private String  descripcion;
    private Long    completado;
    private Long    estatus;
    private Long    Periocidad;
    private String  login;
}
