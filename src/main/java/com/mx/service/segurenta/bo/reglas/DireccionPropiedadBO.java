package com.mx.service.segurenta.bo.reglas;
import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DireccionPropiedadBO  implements Serializable
{
	private static final long serialVersionUID = 8334290105021947380L;
	private Long    id;
    private Long    propiedad;  
    private String  calle;
    private String  numerointerior;
    private String  numeroexterior;
    private String  codigopostal;
    private String  colonia;
    private String  ciudad;
    private String  latitud;
    private String  longitud;
    private String  observaciones;
    private String  zona;
    private String  delegacion;
    private String  login;
}
