package com.mx.service.segurenta.bo.reglas;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class RelacionPropiedadBO 
{
	private Long    id;
	private Long    historial;
	private Long    persona;
	private Long    tipoleacion;
	private String  login;
}
