package com.mx.service.segurenta.bo.reglas;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class PerfilBO 
{
	private Long    id;
	private Long    cuenta;
	private Long    perfilconfig;
	private String  login;
}
