package com.mx.service.segurenta.bo.reglas;
import java.io.Serializable;
import java.sql.Timestamp;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class HistorialPropiedadBO   implements Serializable
{
	private static final long serialVersionUID = 8334290105021947380L;
    private Long      id;
    private Long      propiedad;
    private Long      configuracioncontrato;
	private Timestamp fechainicio;
	private Timestamp fechafinal;
    private Long      estatus;
    private String    poliza;
    private Double    montocomision;
    private Double    porcentajecomision;
    private String    numerocontrato;
    private String    login;
}
