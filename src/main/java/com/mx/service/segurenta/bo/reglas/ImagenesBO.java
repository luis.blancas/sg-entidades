package com.mx.service.segurenta.bo.reglas;
import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ImagenesBO   implements Serializable
{
	private static final long serialVersionUID = 8334290105021947380L;
	private Long    id;
	private String  dato;
    private Long    tipoimagenen;
	private Long    propiedad;
	private String  login;
}
