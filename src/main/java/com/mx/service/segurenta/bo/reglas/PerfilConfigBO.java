package com.mx.service.segurenta.bo.reglas;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class PerfilConfigBO 
{
	private Long    id;
    private Long    modulo;
	private Integer ver;
	private Integer crear;
	private Integer editar;
	private Integer borrado;
	private String  login;
}
