package com.mx.service.segurenta.bo.reglas;
import java.io.Serializable;
import java.sql.Timestamp;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class GarantiasBO   implements Serializable
{
	private static final long serialVersionUID = 8334290105021947380L;
	private Long      id;
    private Long      direccionpropiedad;  
    private Long      relacionpropiedad;
    private String    n_escrita;
    private String    notaria;
    private String    notario;
    private String    no_rpp;
    private Timestamp fecha_escritura;
    private String    login;
}
