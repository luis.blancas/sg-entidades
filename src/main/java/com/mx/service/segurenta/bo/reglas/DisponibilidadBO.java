package com.mx.service.segurenta.bo.reglas;
import java.io.Serializable;
import java.sql.Timestamp;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DisponibilidadBO   implements Serializable
{
	private static final long serialVersionUID = 8334290105021947380L;
	private Long      id;
	private Timestamp fecha;
	private Timestamp inicio;
    private Long      estatus;
	private Long      propiedad;
	private Long      agente;
	private String    login;
}
