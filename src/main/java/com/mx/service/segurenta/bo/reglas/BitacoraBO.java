package com.mx.service.segurenta.bo.reglas;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class BitacoraBO   implements Serializable
{
	private static final long serialVersionUID = 8334290105021947380L;
	private Long    id;
	private Long    cuenta;
    private Long    modulo;
    private String  accion; 
    private String  descripcion;
    private String  login;
}
