package com.mx.service.segurenta.bo.reglas;
import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ConfiguracionContratoBO   implements Serializable
{
	private static final long serialVersionUID = 8334290105021947380L;
	private Long    id;
    private Long    nacionalidad; 
    private Long    tipoarrendatario;  
    private Long    tipoarrendador; 
	private Integer conosiniva;
    private Long    tipoobligadosolidario;  
    private Long    tipoaval;
    private String  login;
}
