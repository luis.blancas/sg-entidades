package com.mx.service.segurenta.bo.reglas;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ReferenciaBO   implements Serializable
{
	private static final long serialVersionUID = 8334290105021947380L;
	private Long    id;
    private Long    persona;  
    private Long    tiporeferencia;
    private String  login;
}
