package com.mx.service.segurenta.bo.reglas;
import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
@Getter
@Setter 
@NoArgsConstructor
@AllArgsConstructor
public class AgenteBO implements Serializable
{
	private Long      id;
    private Long      estatus;
    private Long      persona; 
    private Long      sucursal;
    private String    login;
}
