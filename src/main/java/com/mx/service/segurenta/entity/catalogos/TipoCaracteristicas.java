package com.mx.service.segurenta.entity.catalogos;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.mx.service.segurenta.entity.Users;
import com.mx.service.segurenta.entity.reglas.Persona;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name="TipoCaracteristicas", schema="catalogos")
@Data
@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class TipoCaracteristicas   implements Serializable
{
	private static final long serialVersionUID = 8334290105021947380L;
	@Id
    @SequenceGenerator(
            name = "sequence_tipocaracteristicas",
            		schema="catalogos",
            sequenceName = "sequence_tipocaracteristicas",
            allocationSize = 1)
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "sequence_tipocaracteristicas"
    )
    private Long id;
    private String descripcion;
    @OneToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "idtipodato", referencedColumnName = "id")
    private TipoDato idtipodato;
    @OneToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "idcategoria", referencedColumnName = "id")
    private Categoria idcategoria;
    private Integer conicono;
    private Integer idicono;
    
    //----------------------- Auditoria
    private Timestamp creado;
    @OneToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "creadopor", referencedColumnName = "id")
    private Users creadopor;
    private Timestamp modificado;
    @OneToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "modificadopor", referencedColumnName = "id")
    private Users modificadopor;
	public TipoCaracteristicas(String descripcion,Users user) 
	{
		super();
		this.descripcion = descripcion;
		Date creacion = new Date(System.currentTimeMillis());
        Timestamp  creacionStamp = (new Timestamp(creacion.getTime()));
        this.creadopor =  user;
        this.modificadopor=user;
        this.creado = creacionStamp ;
        this.modificado = creacionStamp ;
        
	}
}
