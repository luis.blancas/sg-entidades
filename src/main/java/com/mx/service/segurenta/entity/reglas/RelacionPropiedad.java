package com.mx.service.segurenta.entity.reglas;

import java.sql.Timestamp;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mx.service.segurenta.entity.catalogos.Modulo;
import com.mx.service.segurenta.entity.catalogos.TipoRelacion;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name="RelacionPropiedad", schema="segurenta")
@Data
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RelacionPropiedad {
	@Id
    @SequenceGenerator(
            name = "sequence_relacionpropiedad",
            		schema="segurenta",
            sequenceName = "sequence_relacionpropiedad",
            allocationSize = 1)
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "sequence_relacionpropiedad"
    )
    private Long id;
	@ManyToOne(fetch =  FetchType.LAZY, optional=false)
	@JoinColumn(name = "idhistorial", nullable= false)
	@OnDelete(action =  OnDeleteAction.CASCADE)
	@JsonIgnore
	private HistorialPropiedad idhistorial;
	
	@ManyToOne(fetch =  FetchType.LAZY, optional=false)
	@JoinColumn(name = "idpersona", nullable= false)
	@OnDelete(action =  OnDeleteAction.CASCADE)
	@JsonIgnore
	private Persona idpersona;
	@OneToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "tipoleacion", referencedColumnName = "id")
	private TipoRelacion tipoleacion;
	
	
}
