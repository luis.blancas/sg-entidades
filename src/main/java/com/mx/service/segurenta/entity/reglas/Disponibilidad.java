package com.mx.service.segurenta.entity.reglas;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mx.service.segurenta.entity.Users;
import com.mx.service.segurenta.entity.catalogos.Estatus;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name="Disponibilidad", schema="segurenta")
@Data
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Disponibilidad   implements Serializable
{
	private static final long serialVersionUID = 8334290105021947380L;
	@Id
    @SequenceGenerator(
            name = "sequence_disponibilidad",
            		schema="segurenta",
            sequenceName = "sequence_disponibilidad",
            allocationSize = 1)
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "sequence_disponibilidad"
    )
    private Long id;
	private Timestamp fecha;
	private Timestamp inicio;
	@OneToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "idestatus", referencedColumnName = "id")
    private Estatus idestatus;
	
	@ManyToOne(fetch =  FetchType.LAZY, optional=false)
	@JoinColumn(name = "idpropiedad", nullable= false)
	@OnDelete(action =  OnDeleteAction.CASCADE)
	@JsonIgnore
	private Propiedad idpropiedad; 
	
	@ManyToOne(fetch =  FetchType.LAZY, optional=false)
	@JoinColumn(name = "idagente", nullable= false)
	@OnDelete(action =  OnDeleteAction.CASCADE)
	@JsonIgnore
	private Agente idagente;
    
    //----------------------- Auditoria
	private Timestamp creado;
    @OneToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "creadopor", referencedColumnName = "id")
    private Users creadopor;
    private Timestamp modificado;
    @OneToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "modificadopor", referencedColumnName = "id")
    private Users modificadopor;
}
