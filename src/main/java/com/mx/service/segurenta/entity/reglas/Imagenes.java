package com.mx.service.segurenta.entity.reglas
;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mx.service.segurenta.entity.Users;
import com.mx.service.segurenta.entity.catalogos.Estatus;
import com.mx.service.segurenta.entity.catalogos.TipoImagen;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name="Imagenes", schema="segurenta")
@Data
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Imagenes   implements Serializable
{
	private static final long serialVersionUID = 8334290105021947380L;
	@Id
    @SequenceGenerator(
            name = "sequence_imagenes",
            		schema="segurenta",
            sequenceName = "sequence_imagenes",
            allocationSize = 1)
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "sequence_imagenes"
    )
    private Long id;
	private String dato;
	@OneToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "idtipoimagenen", referencedColumnName = "id")
    private TipoImagen idtipoimagenen;
	
	@ManyToOne(fetch =  FetchType.LAZY, optional=false)
	@JoinColumn(name = "idpropiedad", nullable= false)
	@OnDelete(action =  OnDeleteAction.CASCADE)
	@JsonIgnore
	private Propiedad idpropiedad; 
	
    
    //----------------------- Auditoria
	private Timestamp creado;
    @OneToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "creadopor", referencedColumnName = "id")
    private Users creadopor;
    private Timestamp modificado;
    @OneToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "modificadopor", referencedColumnName = "id")
    private Users modificadopor;
}
