package com.mx.service.segurenta.entity;

import static java.util.stream.Collectors.toList;

import java.sql.Timestamp;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name="users", schema="oauth")
@Data
@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Users {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    Long id;

    @NotEmpty
    private String username;

    @NotEmpty
    private String password;
    
    private int activo;
    private String nombres;
    private String paterno;
    private String materno;
    private String email;
    private int intentos;
    private int maximo_intentos;
    private int sesionminutos;
    private Timestamp fecha_creacion;
    private Timestamp fecha_cambio_password;
    
}
