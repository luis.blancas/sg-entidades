package com.mx.service.segurenta.entity.reglas;

import java.sql.Timestamp;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.mx.service.segurenta.entity.Users;
import com.mx.service.segurenta.entity.catalogos.Modulo;
import com.mx.service.segurenta.entity.catalogos.TipoUsuario;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name="PermisoConfig", schema="segurenta")
@Data
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PermisoConfig {
	@Id
    @SequenceGenerator(
            name = "sequence_permisoconfig",
            		schema="segurenta",
            sequenceName = "sequence_permisoconfig",
            allocationSize = 1)
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "sequence_permisoconfig"
    )
    private Long id;
	@OneToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "idmodulo", referencedColumnName = "id")
    private Modulo idmodulo;
	@OneToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "idtipousuario", referencedColumnName = "id")
    private TipoUsuario idtipousuario;
	private Integer ver;
	private Integer crear;
	private Integer editar;
	private Integer borrado;
	
	//----------------------- Auditoria
	private Timestamp creado;
    @OneToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "creadopor", referencedColumnName = "id")
    private Users creadopor;
    private Timestamp modificado;
    @OneToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "modificadopor", referencedColumnName = "id")
    private Users modificadopor;
}
