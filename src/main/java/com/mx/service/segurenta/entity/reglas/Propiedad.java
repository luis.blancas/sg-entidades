package com.mx.service.segurenta.entity.reglas;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.mx.service.segurenta.entity.Users;
import com.mx.service.segurenta.entity.catalogos.Estatus;
import com.mx.service.segurenta.entity.catalogos.Periocidad;
import com.mx.service.segurenta.entity.catalogos.TipoPropiedad;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name="Propiedad", schema="segurenta")
@Data
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Propiedad   implements Serializable
{
	private static final long serialVersionUID = 8334290105021947380L;
	@Id
    @SequenceGenerator(
            name = "sequence_propiedad",
            		schema="segurenta",
            sequenceName = "sequence_propiedad",
            allocationSize = 1)
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "sequence_propiedad"
    )
    private Long id;
	@OneToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "idtipopropiedad", referencedColumnName = "id")
    private TipoPropiedad idtipopropiedad;
    private String nombre;
    private Double precio;
    private Double preciorenta;
    private Double mantenimiento;
    private String descripcion;
    private Integer completado;
    @OneToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "idcuenta", referencedColumnName = "id")
    private Cuenta  idcuenta;
    @OneToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "idestatus", referencedColumnName = "id")
    private Estatus  idestatus;
    @OneToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "idPeriocidad", referencedColumnName = "id")
    private Periocidad  idPeriocidad;
    //----------------------- Auditoria
    private Timestamp creado;
    @OneToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "creadopor", referencedColumnName = "id")
    private Users creadopor;
    private Timestamp modificado;
    @OneToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "modificadopor", referencedColumnName = "id")
    private Users modificadopor;
}
