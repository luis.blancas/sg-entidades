package com.mx.service.segurenta.entity.reglas;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.mx.service.segurenta.entity.Users;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name="Garantias", schema="segurenta")
@Data
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Garantias   implements Serializable
{
	private static final long serialVersionUID = 8334290105021947380L;
	@Id
    @SequenceGenerator(
            name = "sequence_garantias",
            		schema="segurenta",
            sequenceName = "sequence_garantias",
            allocationSize = 1)
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "sequence_garantias"
    )
    private Long id;
	@OneToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "iddireccionpropiedad", referencedColumnName = "id")
    private DireccionPropiedad iddireccionpropiedad;  
	
	@OneToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "idrelacionpropiedad", referencedColumnName = "id")
    private RelacionPropiedad idrelacionpropiedad;
	
	
    private String n_escrita;
    private String notaria;
    private String notario;
    private String no_rpp;
    private Timestamp fecha_escritura;
    //----------------------- Auditoria
    private Timestamp creado;
    @OneToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "creadopor", referencedColumnName = "id")
    private Users creadopor;
    private Timestamp modificado;
    @OneToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "modificadopor", referencedColumnName = "id")
    private Users modificadopor;
}
