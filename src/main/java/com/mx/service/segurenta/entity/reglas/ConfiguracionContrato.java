package com.mx.service.segurenta.entity.reglas;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.mx.service.segurenta.entity.Users;
import com.mx.service.segurenta.entity.catalogos.Nacionalidad;
import com.mx.service.segurenta.entity.catalogos.TipoArrendador;
import com.mx.service.segurenta.entity.catalogos.TipoArrendatario;
import com.mx.service.segurenta.entity.catalogos.TipoAval;
import com.mx.service.segurenta.entity.catalogos.TipoObligadoSolidario;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name="ConfiguracionContrato", schema="segurenta")
@Data
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ConfiguracionContrato   implements Serializable
{
	private static final long serialVersionUID = 8334290105021947380L;
	@Id
    @SequenceGenerator(
            name = "sequence_configuracioncontrato",
            		schema="segurenta",
            sequenceName = "sequence_configuracioncontrato",
            allocationSize = 1)
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "sequence_configuracioncontrato"
    )
    private Long id;
	@OneToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "idnacionalidad", referencedColumnName = "id")
    private Nacionalidad idnacionalidad; 
	@OneToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "idtipoarrendatario", referencedColumnName = "id")
    private TipoArrendatario idtipoarrendatario;  
	@OneToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "idtipoarrendador", referencedColumnName = "id")
    private TipoArrendador idtipoarrendador; 
	private Integer conosiniva;
	@OneToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "idtipoobligadosolidario", referencedColumnName = "id")
    private TipoObligadoSolidario idtipoobligadosolidario;  
	@OneToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "idtipoaval", referencedColumnName = "id")
    private TipoAval idtipoaval;  
    
    //----------------------- Auditoria
	private Timestamp creado;
    @OneToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "creadopor", referencedColumnName = "id")
    private Users creadopor;
    private Timestamp modificado;
    @OneToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "modificadopor", referencedColumnName = "id")
    private Users modificadopor;
}
