package com.mx.service.segurenta;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ServiceCatalogosApplication {

	public static void main(String[] args) {
		SpringApplication.run(ServiceCatalogosApplication.class, args);
	}

}
