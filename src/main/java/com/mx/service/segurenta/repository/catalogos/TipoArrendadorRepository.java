package com.mx.service.segurenta.repository.catalogos;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mx.service.segurenta.entity.catalogos.TipoArrendador;
/**
 * <code>EstatusRepository</code>.
 *
 * @author ${eduardo.santiago}
 * @version 1.0
 */
public interface TipoArrendadorRepository extends JpaRepository<TipoArrendador, Long> 
{
	Optional<TipoArrendador> findById(Long id);
	List<TipoArrendador> findAll();
}
