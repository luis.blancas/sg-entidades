package com.mx.service.segurenta.repository.reglas;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import com.mx.service.segurenta.entity.reglas.ComisionAgente;
public interface ComisionAgenteRepository extends JpaRepository<ComisionAgente, Long> 
{
	Optional<ComisionAgente> findById(Long id);
	List<ComisionAgente> findAll();
}
