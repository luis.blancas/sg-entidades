package com.mx.service.segurenta.repository.catalogos;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mx.service.segurenta.entity.catalogos.TipoPropiedad;
/**
 * <code>EstatusRepository</code>.
 *
 * @author ${eduardo.santiago}
 * @version 1.0
 */
public interface TipoPropiedadRepository extends JpaRepository<TipoPropiedad, Long> 
{
	Optional<TipoPropiedad> findById(Long id);
	List<TipoPropiedad> findAll();
}
