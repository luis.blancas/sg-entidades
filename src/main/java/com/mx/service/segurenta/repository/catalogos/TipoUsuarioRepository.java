package com.mx.service.segurenta.repository.catalogos;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mx.service.segurenta.entity.catalogos.TipoUsuario;
/**
 * <code>EstatusRepository</code>.
 *
 * @author ${eduardo.santiago}
 * @version 1.0
 */
public interface TipoUsuarioRepository extends JpaRepository<TipoUsuario, Long> 
{
	Optional<TipoUsuario> findById(Long id);
	List<TipoUsuario> findAll();
}
