package com.mx.service.segurenta.repository.reglas;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import com.mx.service.segurenta.entity.reglas.Direccion;
public interface DireccionRepository  extends JpaRepository<Direccion, Long> 
{
	Optional<Direccion> findById(Long id);
	List<Direccion> findAll();
}
