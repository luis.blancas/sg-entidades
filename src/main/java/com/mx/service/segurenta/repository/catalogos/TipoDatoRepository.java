package com.mx.service.segurenta.repository.catalogos;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mx.service.segurenta.entity.catalogos.TipoDato;
/**
 * <code>EstatusRepository</code>.
 *
 * @author ${eduardo.santiago}
 * @version 1.0
 */
public interface TipoDatoRepository extends JpaRepository<TipoDato, Long> 
{
	Optional<TipoDato> findById(Long id);
	List<TipoDato> findAll();
}
