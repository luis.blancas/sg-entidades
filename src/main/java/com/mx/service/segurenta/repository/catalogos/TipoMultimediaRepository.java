package com.mx.service.segurenta.repository.catalogos;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mx.service.segurenta.entity.catalogos.TipoMultimedia;
/**
 * <code>EstatusRepository</code>.
 *
 * @author ${eduardo.santiago}
 * @version 1.0
 */
public interface TipoMultimediaRepository extends JpaRepository<TipoMultimedia, Long> 
{
	Optional<TipoMultimedia> findById(Long id);
	List<TipoMultimedia> findAll();
}
