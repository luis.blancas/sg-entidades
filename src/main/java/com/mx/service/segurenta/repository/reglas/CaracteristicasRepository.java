package com.mx.service.segurenta.repository.reglas;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import com.mx.service.segurenta.entity.reglas.Caracteristicas;
public interface CaracteristicasRepository extends JpaRepository<Caracteristicas, Long> 
{
	Optional<Caracteristicas> findById(Long id);
	List<Caracteristicas> findAll();
}
