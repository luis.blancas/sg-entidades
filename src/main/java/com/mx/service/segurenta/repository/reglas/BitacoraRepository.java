package com.mx.service.segurenta.repository.reglas;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import com.mx.service.segurenta.entity.reglas.Bitacora;
public interface BitacoraRepository extends JpaRepository<Bitacora, Long> 
{
	Optional<Bitacora> findById(Long id);
	List<Bitacora> findAll();
}
