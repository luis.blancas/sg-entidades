package com.mx.service.segurenta.repository.catalogos;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mx.service.segurenta.entity.catalogos.TipoArrendatario;
/**
 * <code>EstatusRepository</code>.
 *
 * @author ${eduardo.santiago}
 * @version 1.0
 */
public interface TipoArrendatarioRepository extends JpaRepository<TipoArrendatario, Long> 
{
	Optional<TipoArrendatario> findById(Long id);
	List<TipoArrendatario> findAll();
}
