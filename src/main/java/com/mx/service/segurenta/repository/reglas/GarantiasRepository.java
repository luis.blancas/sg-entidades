package com.mx.service.segurenta.repository.reglas;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import com.mx.service.segurenta.entity.reglas.Garantias;
public interface GarantiasRepository  extends JpaRepository<Garantias, Long> 
{
	Optional<Garantias> findById(Long id);
	List<Garantias> findAll();
}
