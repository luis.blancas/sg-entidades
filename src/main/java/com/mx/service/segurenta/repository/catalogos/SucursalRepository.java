package com.mx.service.segurenta.repository.catalogos;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mx.service.segurenta.entity.catalogos.Sucursal;
/**
 * <code>EstatusRepository</code>.
 *
 * @author ${eduardo.santiago}
 * @version 1.0
 */
public interface SucursalRepository extends JpaRepository<Sucursal, Long> 
{
	Optional<Sucursal> findById(Long id);
	List<Sucursal> findAll();
}
