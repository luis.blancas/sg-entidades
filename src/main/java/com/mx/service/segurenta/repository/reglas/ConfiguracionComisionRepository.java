package com.mx.service.segurenta.repository.reglas;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import com.mx.service.segurenta.entity.reglas.ConfiguracionComision;
public interface ConfiguracionComisionRepository extends JpaRepository<ConfiguracionComision, Long> 
{
	Optional<ConfiguracionComision> findById(Long id);
	List<ConfiguracionComision> findAll();
}
