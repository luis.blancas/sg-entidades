package com.mx.service.segurenta.repository.reglas;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import com.mx.service.segurenta.entity.reglas.RelacionPropiedad;
public interface RelacionPropiedadRepository extends JpaRepository<RelacionPropiedad, Long> 
{
	Optional<RelacionPropiedad> findById(Long id);
	List<RelacionPropiedad> findAll();
}

