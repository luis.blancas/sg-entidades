package com.mx.service.segurenta.repository.reglas;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import com.mx.service.segurenta.entity.reglas.IndividualPersona;
public interface IndividualPersonaRepository extends JpaRepository<IndividualPersona, Long> 
{
	Optional<IndividualPersona> findById(Long id);
	List<IndividualPersona> findAll();
}

