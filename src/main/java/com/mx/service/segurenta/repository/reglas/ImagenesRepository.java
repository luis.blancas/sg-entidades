package com.mx.service.segurenta.repository.reglas;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import com.mx.service.segurenta.entity.reglas.Imagenes;
public interface ImagenesRepository  extends JpaRepository<Imagenes, Long> 
{
	Optional<Imagenes> findById(Long id);
	List<Imagenes> findAll();
}
