package com.mx.service.segurenta.repository.catalogos;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mx.service.segurenta.entity.catalogos.TipoAval;
/**
 * <code>EstatusRepository</code>.
 *
 * @author ${eduardo.santiago}
 * @version 1.0
 */
public interface TipoAvalRepository extends JpaRepository<TipoAval, Long> 
{
	Optional<TipoAval> findById(Long id);
	List<TipoAval> findAll();
}
