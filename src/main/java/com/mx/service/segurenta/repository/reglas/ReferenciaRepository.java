package com.mx.service.segurenta.repository.reglas;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import com.mx.service.segurenta.entity.reglas.Referencia;
public interface ReferenciaRepository  extends JpaRepository<Referencia, Long> 
{
	Optional<Referencia> findById(Long id);
	List<Referencia> findAll();
}

