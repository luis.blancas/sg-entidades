package com.mx.service.segurenta.repository.catalogos;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mx.service.segurenta.entity.catalogos.TipoImagen;
/**
 * <code>EstatusRepository</code>.
 *
 * @author ${eduardo.santiago}
 * @version 1.0
 */
public interface TipoImagenRepository extends JpaRepository<TipoImagen, Long> 
{
	Optional<TipoImagen> findById(Long id);
	List<TipoImagen> findAll();
}
