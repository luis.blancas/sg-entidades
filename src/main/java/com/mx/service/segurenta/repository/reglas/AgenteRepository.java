package com.mx.service.segurenta.repository.reglas;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import com.mx.service.segurenta.entity.reglas.Agente;
public interface AgenteRepository extends JpaRepository<Agente, Long> 
{
	Optional<Agente> findById(Long id);
	List<Agente> findAll();
}
