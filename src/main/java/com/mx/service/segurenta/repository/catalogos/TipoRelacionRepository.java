package com.mx.service.segurenta.repository.catalogos;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mx.service.segurenta.entity.catalogos.TipoRelacion;
/**
 * <code>EstatusRepository</code>.
 *
 * @author ${eduardo.santiago}
 * @version 1.0
 */
public interface TipoRelacionRepository extends JpaRepository<TipoRelacion, Long> 
{
	Optional<TipoRelacion> findById(Long id);
	List<TipoRelacion> findAll();
}
