package com.mx.service.segurenta.repository.reglas;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import com.mx.service.segurenta.entity.reglas.JuridicalPersona;
public interface JuridicalPersonaRepository  extends JpaRepository<JuridicalPersona, Long> 
{
	Optional<JuridicalPersona> findById(Long id);
	List<JuridicalPersona> findAll();
}

