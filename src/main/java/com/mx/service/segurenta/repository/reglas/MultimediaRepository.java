package com.mx.service.segurenta.repository.reglas;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import com.mx.service.segurenta.entity.reglas.Multimedia;
public interface MultimediaRepository  extends JpaRepository<Multimedia, Long> 
{
	Optional<Multimedia> findById(Long id);
	List<Multimedia> findAll();
}

