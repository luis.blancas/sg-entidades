package com.mx.service.segurenta.repository.reglas;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import com.mx.service.segurenta.entity.reglas.Propiedad;
public interface PropiedadRepository extends JpaRepository<Propiedad, Long> 
{
	Optional<Propiedad> findById(Long id);
	List<Propiedad> findAll();
}
