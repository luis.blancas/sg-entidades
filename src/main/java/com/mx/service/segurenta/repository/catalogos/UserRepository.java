package com.mx.service.segurenta.repository.catalogos;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mx.service.segurenta.entity.Users;
public interface UserRepository extends JpaRepository<Users, Long> 
{
    Optional<Users> findByUsername(String username);
}
