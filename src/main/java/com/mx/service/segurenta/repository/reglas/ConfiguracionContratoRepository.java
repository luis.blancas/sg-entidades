package com.mx.service.segurenta.repository.reglas;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import com.mx.service.segurenta.entity.reglas.ConfiguracionContrato;
public interface ConfiguracionContratoRepository extends JpaRepository<ConfiguracionContrato, Long> 
{
	Optional<ConfiguracionContrato> findById(Long id);
	List<ConfiguracionContrato> findAll();
}
