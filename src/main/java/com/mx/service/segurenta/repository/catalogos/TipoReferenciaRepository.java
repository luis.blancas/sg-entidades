package com.mx.service.segurenta.repository.catalogos;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mx.service.segurenta.entity.catalogos.TipoReferencia;
/**
 * <code>EstatusRepository</code>.
 *
 * @author ${eduardo.santiago}
 * @version 1.0
 */
public interface  TipoReferenciaRepository extends JpaRepository<TipoReferencia, Long> 
{
	Optional<TipoReferencia> findById(Long id);
	List<TipoReferencia> findAll();
}
