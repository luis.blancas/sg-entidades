package com.mx.service.segurenta.repository.reglas;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import com.mx.service.segurenta.entity.reglas.Cuenta;
public interface CuentaRepository extends JpaRepository<Cuenta, Long> 
{
	Optional<Cuenta> findById(Long id);
	List<Cuenta> findAll();
}
