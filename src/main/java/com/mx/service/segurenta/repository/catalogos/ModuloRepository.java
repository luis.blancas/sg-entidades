package com.mx.service.segurenta.repository.catalogos;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mx.service.segurenta.entity.catalogos.Modulo;
/**
 * <code>EstatusRepository</code>.
 *
 * @author ${eduardo.santiago}
 * @version 1.0
 */
public interface ModuloRepository extends JpaRepository<Modulo, Long> 
{
	Optional<Modulo> findById(Long id);
	List<Modulo> findAll();
}
