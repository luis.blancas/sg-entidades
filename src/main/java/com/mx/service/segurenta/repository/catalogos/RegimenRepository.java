package com.mx.service.segurenta.repository.catalogos;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mx.service.segurenta.entity.catalogos.Regimen;
/**
 * <code>EstatusRepository</code>.
 *
 * @author ${eduardo.santiago}
 * @version 1.0
 */
public interface RegimenRepository extends JpaRepository<Regimen, Long> 
{
	Optional<Regimen> findById(Long id);
	List<Regimen> findAll();
}
