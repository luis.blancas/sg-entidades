package com.mx.service.segurenta.repository.catalogos;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mx.service.segurenta.entity.catalogos.Periocidad;
/**
 * <code>EstatusRepository</code>.
 *
 * @author ${eduardo.santiago}
 * @version 1.0
 */
public interface PeriocidadRepository extends JpaRepository<Periocidad, Long> 
{
	Optional<Periocidad> findById(Long id);
	List<Periocidad> findAll();
}
