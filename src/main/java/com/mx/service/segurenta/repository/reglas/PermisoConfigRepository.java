package com.mx.service.segurenta.repository.reglas;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import com.mx.service.segurenta.entity.reglas.PermisoConfig;
public interface PermisoConfigRepository  extends JpaRepository<PermisoConfig, Long> 
{
	Optional<PermisoConfig> findById(Long id);
	List<PermisoConfig> findAll();
}

