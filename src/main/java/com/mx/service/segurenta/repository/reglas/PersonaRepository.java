package com.mx.service.segurenta.repository.reglas;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import com.mx.service.segurenta.entity.reglas.Persona;
public interface PersonaRepository  extends JpaRepository<Persona, Long> 
{
	Optional<Persona> findById(Long id);
	List<Persona> findAll();
}
