package com.mx.service.segurenta.repository.reglas;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import com.mx.service.segurenta.entity.reglas.DireccionPropiedad;
public interface DireccionPropiedadRepository  extends JpaRepository<DireccionPropiedad, Long> 
{
	Optional<DireccionPropiedad> findById(Long id);
	List<DireccionPropiedad> findAll();
}
