package com.mx.service.segurenta.repository.catalogos;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mx.service.segurenta.entity.catalogos.Nacionalidad;
/**
 * <code>EstatusRepository</code>.
 *
 * @author ${eduardo.santiago}
 * @version 1.0
 */
public interface NacionalidadRepository extends JpaRepository<Nacionalidad, Long> 
{
	Optional<Nacionalidad> findById(Long id);
	List<Nacionalidad> findAll();
}
