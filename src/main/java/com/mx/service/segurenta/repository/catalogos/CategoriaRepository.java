package com.mx.service.segurenta.repository.catalogos;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mx.service.segurenta.entity.catalogos.Categoria;
/**
 * <code>EstatusRepository</code>.
 *
 * @author ${eduardo.santiago}
 * @version 1.0
 */
public interface CategoriaRepository extends JpaRepository<Categoria, Long> 
{
	Optional<Categoria> findById(Long id);
	List<Categoria> findAll();
}
